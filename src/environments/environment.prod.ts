export const environment = {
production: true,

  url: 'http://fishtech.co.kr:3000/api',
  homepage: 'http://fishtech.co.kr',

  username: 'username',
  jwt_token: 'jwt_token',
  company_id: 'company_id',
  user_id: 'user_id',

  msg_ok: '정상측정중',
  msg_network_error: '통신불량',
  msg_correction: '교정중',
  msg_replace: '교체중',
  msg_trouble: '고장',
  msg_clean: '세척중',
  msg_range_off: '범위이탈',

  
};
