import { Component, QueryList, ViewChildren, OnInit } from '@angular/core';
import { Platform, AlertController, IonRouterOutlet, ToastController, IonApp } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { AuthenticationService } from './shared/services/authentication.service';
import { Network } from '@ionic-native/network/ngx';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  lastBack: number;
  allowClose: boolean;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthenticationService,
    public alertCtrl: AlertController,
    private router: Router,
    private network: Network,
    private toastCtrl: ToastController,
    private swUpdate: SwUpdate
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.checkToken();  
      this.network.onDisconnect().subscribe(() => {
        this.networkAlert();
        setInterval(() => {
          navigator['app'].exitApp();
        }, 2000);
      });
    });
    
    this.backButtonEvent();
  }

  async displayNetworkUpdate(connectionState: string){
    let networkType = this.network.type;
    const toast = await this.toastCtrl.create({
      message: `You are now ${connectionState} via ${networkType}`,
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  async networkAlert() {
    const alert = await this.alertCtrl.create({
      header: '알림',
      message: '네트워크 연결 확인 바랍니다.',
      buttons: ['확인']
    });
    await alert.present();
  }

  checkToken() {
    this.authService.checkToken();
  }

  // active hardware back button
  backButtonEvent() {
    
    const closeDelay = 2000;
    const spamDelay = 500;

    this.platform.backButton.subscribe(async () => {
      this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
        if (this.router.url === '/home' || this.router.url === '/login' || this.router.url === '/') {
          if (outlet && outlet.canGoBack()) {
            outlet.pop();
          } else if(Date.now() - this.lastBack > spamDelay && !this.allowClose) {
            this.allowClose = true;
            this.presentToast('뒤로버튼 한번 더 누르시면 종료됩니다.', closeDelay);
            
          } else if(Date.now() - this.lastBack < closeDelay && this.allowClose) {
            navigator['app'].exitApp();
          }
            
          this.lastBack = Date.now();
        }
      });
    });
  }

  async presentToast(msg, closeDelay) {
    const toast = await this.toastCtrl.create({
        message: msg,
        position: 'bottom',
        duration: closeDelay
    });
    toast.onDidDismiss().then(() => {
			this.allowClose = false;
    });
    toast.present();
  }


}
