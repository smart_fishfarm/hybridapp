import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class GetApiService {

  url = environment.url;
  TOKEN_NAME = environment.jwt_token;

  public headers: any;

  constructor(
    private http: HttpClient,
    private storage: Storage
  ) {

    this.storage.get(this.TOKEN_NAME).then(token => {
      if (token) {
        this.headers = new HttpHeaders()
        .set('Authorization', 'Bearer ' + token);
      }
    });

  }

  getCompany(company_id) {
    return this.http.get(`${this.url}/company/` + company_id, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getRealtime(company_id) {
    return this.http.get(`${this.url}/realtime/` + company_id, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getAnalyzer(analyzer_id) {
    return this.http.get(`${this.url}/analyzer/` + analyzer_id, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getAllAnalyzer(company_id) {
    return this.http.get(`${this.url}/analyzer-all/` + company_id, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getProfile(username) {
    const params = new HttpParams().set('username', username);
    return this.http.get(`${this.url}/profile`, { headers: this.headers, params: params }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }
  
  getChart(analyzerId, userId, today, yesterday) {
    const params = new HttpParams()
    .set('analyzer_id', analyzerId)
    .set('user_id', userId)
    .set('today', today)
    .set('yesterday', yesterday);

    return this.http.get(`${this.url}/chart`, { headers: this.headers, params: params }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  updateChart(id, start, end) {
    const params = new HttpParams()
    .set('start', start)
    .set('end', end);    
    return this.http.get(`${this.url}/chart/update/`+id, { headers: this.headers, params: params }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }
  
  getTimeseries(analyzerId, userId, start, end) {
    const params = new HttpParams()
                  .set('analyzer_id', analyzerId)
                  .set('user_id', userId)
                  .set('start', start)
                  .set('end', end);
    return this.http.get(`${this.url}/timeseries`, { headers: this.headers, params: params }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getInfoBasic(id) {
    return this.http.get(`${this.url}/info/${id}`, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getInfoCondition(id) {
    return this.http.get(`${this.url}/info/condition/${id}`, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getAllLimits(userid) {
    return this.http.get(`${this.url}/all-limits/`+userid, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getInfoLimits(analyzerId, userId) {

    return this.http.get(`${this.url}/info/limits/${analyzerId}/${userId}`, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getInfoSchedule(id) {
    return this.http.get(`${this.url}/info/schedule/` + id, { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }
  
  getNotification(companyid, userid) {
    return this.http.get(`${this.url}/notification/${companyid}/${userid}` , { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getPushSetting(userid) {
    return this.http.get(`${this.url}/push-setting/${userid}` , { headers: this.headers }).pipe(
      map((res: any) => {
        return res[0];
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

  getNotiCount(companyid, userid) {
    return this.http.get(`${this.url}/notification/count/${companyid}/${userid}` , { headers: this.headers }).pipe(
      map((res: any) => {
        return res;
      }),
      catchError(e => {
        throw new Error(e);
      })
    );
  }

}
