import { AlertController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { GetApiService } from 'src/app/shared/services/get-api.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { _ } from 'underscore';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {

  COMPANY_ID = environment.company_id;
  USER_ID = environment.user_id;

  MSG_OK = environment.msg_ok;
  MSG_NETWORK_ERROR = environment.msg_network_error;
  MSG_CORRECTION = environment.msg_correction;
  MSG_REPLACE = environment.msg_replace;
  MSG_TROUBLE = environment.msg_trouble;
  MSG_CLEAN = environment.msg_clean;

  groups: any;
  objectKeys = Object.keys;

  isEditItems: boolean;
  searchText: any;

  constructor(
    private getapi: GetApiService,
    private storage: Storage,
    public alertCtrl: AlertController,
    private toastController: ToastController,

  ) {

   }

  ngOnInit() {

    this.storage.get(this.COMPANY_ID).then(companyId => {
      this.storage.get(this.USER_ID).then(userId => {
        this.getapi.getNotification(companyId, userId).subscribe((res: any) => { 
          
          this.groups = res;      
        });
      });
    });

  }


  eventPrompt(slidingItem: HTMLIonItemSlidingElement) {
    //this.isEditItems = !this.isEditItems;
    this.isEditItems = !this.isEditItems;
    slidingItem.close();

  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
        message: msg,
        position: 'bottom',
        duration: 2500
    });
    toast.present();
  }

  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

    }, 1000);
  }


}
