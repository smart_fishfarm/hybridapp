import { Storage } from '@ionic/storage';
import { GetApiService } from './../../shared/services/get-api.service';
import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { PostApiService } from 'src/app/shared/services/post-api.service';

@Component({
  selector: 'app-push-setting',
  templateUrl: './push-setting.page.html',
  styleUrls: ['./push-setting.page.scss'],
})
export class PushSettingPage implements OnInit {

  USER_ID = environment.user_id;

  onoff: boolean;
  state: boolean;
  limits: boolean;

  constructor(
    private getapi: GetApiService,
    private postapi: PostApiService,
    private storage: Storage,
  ) {
    
  }

  ngOnInit() {
    this.storage.get(this.USER_ID).then(userId => {
      this.getapi.getPushSetting(userId).subscribe((res: any) => { 
        this.onoff = res.onoff;
        this.state = res.state;
        this.limits = res.limits;
      });
    });      
  }

  changeToggle(column, bool) {
    const value = {column: column, bool: bool};
    
    this.storage.get(this.USER_ID).then(userId => {
      this.postapi.updateAlarms(userId, value).subscribe();
    });
  }

}
