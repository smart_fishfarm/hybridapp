import { GetApiService } from './../../shared/services/get-api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { IonSlides, NavController, IonContent } from '@ionic/angular';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  @ViewChild(IonContent) content: IonContent;

  @ViewChild('slider') slider: IonSlides;
  analyzerId : string;
  progress = 0;
  private interval = null;
  title: string;
  segment = "0";
  slideOpts: any = {allowTouchMove: false};


  constructor(
    private activatedRoute: ActivatedRoute,
    public location: Location,
    private getapi: GetApiService,
    private navCtrl: NavController,

  ) {
    this.analyzerId = this.activatedRoute.snapshot.paramMap.get('analyzerId');
    this.getapi.getAnalyzer(this.analyzerId).subscribe((res: any) => {
      this.title = res[0].name_tag;
    });

    this.interval = setInterval(() => {
      if(this.progress < .9) {
        this.progress += .1;
      } else {
        clearInterval(this.interval);
        this.progress = 0;
      }
    }, 50);

  }

  ngOnInit() {
  }

  goBack() {
    this.location.back();
    //this.navCtrl.pop();
  }

  selectedTab(ind){
    this.slider.slideTo(ind);
  }

  moveButton($event) {
    this.segment = $event.target.swiper.activeIndex.toString();
  }

  ScrollToBottom(){
    this.content.scrollToBottom(1500);
  }

  ScrollToTop(){
    this.content.scrollToTop(1500);
  }

}
