import { Storage } from '@ionic/storage';
import { environment } from './../../../../../environments/environment.prod';
import { AlertController, LoadingController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { GetApiService } from 'src/app/shared/services/get-api.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';

/** echarts extensions: */
import 'echarts/theme/macarons.js';
import 'echarts/dist/extension/bmap.min.js';

import * as moment from 'moment';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnInit {

  USER_ID = environment.user_id;

  timestamp : any = [];
  value : any = [];
  value_temp : any = [];
  value_ph : any = [];

  analyzerId : string;
  tableName : string;

  today : string;
  yesterday : string;

  updateOption: any;
  updateOptionPh: any;
  updateOptionTemp: any;

  chartForm: FormGroup;
  
  optionValue = {
    color: ['#80ccff'],
    title: [{
      left: 'center',
      text: '용존산소량'
    }],
    tooltip: {
      trigger: 'axis',
      axisPointer: {
          type: 'line',
          crossStyle: {
              color: '#999'
          }
      }
    },
    dataZoom: [
      {
          show: true,
          realtime: true,
          start: 30,
          end: 70,
      }
    ],
    xAxis: [
      {
        type: 'category',
        boundaryGap : false,
        scale: true,
        data: this.timestamp,
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: 'mg/L',
        splitArea: {
          show: true
        },
      }
    ],
    series: [
      {
        name: 'DO',
        type: 'line',
        barWidth: '60%',
        symbol: 'none',
        data: this.value,
        markPoint: {
          data: [
              {type: 'max', name: '최고점'},
              {type: 'min', name: '최저점'}
          ]
        }
      }
    ]
  };

  optionPh = {
    color: ['#cc00ff'],
    title: [{
      left: 'center',
      text: '수소이온'
    }],
    tooltip: {
      trigger: 'axis'
    },
    dataZoom: [
      {
          show: true,
          realtime: true,
          start: 30,
          end: 70,
      }
    ],
    xAxis: [
      {
        type: 'category',
        boundaryGap : false,
        scale: true,
        data: this.timestamp,
      }
    ],
    yAxis: [
      {
        name: 'pH',
        type: 'value',
        splitArea: {
          show: true
        },
      }
    ],
    series: [
      {
        name: 'pH',
        type: 'line',
        symbol: 'none',
        barWidth: '60%',
        data: this.value_ph,    
        markPoint: {
          data: [
              {type: 'max', name: '최고점'},
              {type: 'min', name: '최저점'}
          ]
        },
      }
    ]
  };

  optionTemp = {
    color: ['#33ff33'],
    title: [{
      left: 'center',
      text: '온도'
    }],
    tooltip: {
      trigger: 'axis'
    },
    dataZoom: [
      {
          show: true,
          realtime: true,
          start: 30,
          end: 70,
      }
    ],
    xAxis: [
      {
        type: 'category',
        boundaryGap : false,
        scale: true,
        data: this.timestamp,
      }
    ],
    yAxis: [
      {
        name: '°C',
        type: 'value',
        splitArea: {
          show: true
        },
      }
    ],
    series: [
      {
        name: 'Temp',
        type: 'line',
        symbol: 'none',
        barWidth: '60%',
        data: this.value_temp,    
        markPoint: {
          data: [
              {type: 'max', name: '최고점'},
              {type: 'min', name: '최저점'}
          ]
        },
      }
    ]
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private getapi: GetApiService,
    private formBuilder: FormBuilder,
    private alertController: AlertController,
    private storage: Storage,
    public loadingController: LoadingController,

  ) {
    this.analyzerId = this.activatedRoute.snapshot.paramMap.get('analyzerId');

    this.today = moment().format("YYYY-MM-DD");
    this.yesterday = moment().subtract(1, 'days').format("YYYY-MM-DD");
    
    this.chartForm =  this.formBuilder.group({
      start: [this.yesterday, [Validators.required]],
      end: [this.today, [Validators.required]],
    });

  }

  ngOnInit() {

    this.getChart(this.analyzerId , this.today, this.yesterday);

    this.storage.ready().then(() => {
      this.storage.get(this.USER_ID).then(userId => {
        this.getapi.getInfoBasic(this.analyzerId).subscribe((info: any) => {
          this.getapi.getInfoLimits(this.analyzerId, userId).subscribe((limits: any) => {
            
            let rArray = [];
            if(limits[0].bool_max) {
              rArray.push({name: '최대', type:'max', yAxis: limits[0].value_max, itemStyle:{normal:{color:'#8B4513'}}});
            }
            if(limits[0].bool_min) {
              rArray.push({name: '최소', type:'min', yAxis: limits[0].value_min, itemStyle:{normal:{color:'#8B4513'}}});
            }
            
          });
        });
      });
    });
  }


  getChart(analyzerId, today, yesterday) {
    this.storage.get(this.USER_ID).then(userId => {
      this.getapi.getChart(analyzerId, userId, today, yesterday).subscribe((data: any) => {
        for(let i=0; i<data.length; i++) {
          let dateString = moment.unix(data[i].timestamp).format("MM/DD HH:mm");
          this.timestamp.push(dateString);
          this.value.push(data[i].value);
          this.value_ph.push(data[i].ph);
          this.value_temp.push(data[i].temp);
        }
      });
    });
  }

  onSubmit() {

    const startDate = this.chartForm.controls['start'].value;
    const endDate = this.chartForm.controls['end'].value;
    
    if (startDate > endDate) {
      this.showAlert("날짜설정이 잘못되었습니다.");
      return;
    }

    let start = startDate.split("T", 1)[0];
    let end = endDate.split("T", 1)[0];

    this.getapi.updateChart(this.analyzerId, start, end)
    .subscribe((data: any) => {
      if(data[0] == null) {
        this.showAlert("데이터가 존재하지 않습니다.");
        return;
      } else {

        this.presentLoading();

        this.timestamp = [];
        this.value = [];
        this.value_ph = [];
        this.value_temp = [];

        
        for(let i=0; i<data.length; i++) {        
          let dateString = moment.unix(data[i].timestamp).format("MM/DD HH:mm");
          this.timestamp.push(dateString);
          this.value.push(data[i].value);
          this.value_ph.push(data[i].ph);
          this.value_temp.push(data[i].temp);
          
        }
        this.updateOption = {
          series: [{
            data: this.value
          }],
          xAxis: [
          {
            data: this.timestamp
          }]
        };

        this.updateOptionPh = {
          series: [{
            data: this.value_ph
          }],
          xAxis: [
          {
            data: this.timestamp
          }]
        };

        this.updateOptionTemp = {
          series: [{
            data: this.value_temp
          }],
          xAxis: [
          {
            data: this.timestamp
          }]
        };


      }
    });

  }

  showAlert(msg) {
    let alert = this.alertController.create({
      message: msg,
      header: '알림',
      buttons: ['OK']
    });
    alert.then(alert => alert.present());
  }


  async presentLoading() {
    const loading = await this.loadingController.create({
      duration: 500,
      message: '로딩중...',
      spinner: 'bubbles',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }

  

}
