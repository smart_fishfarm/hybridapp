import { AlertController, ToastController, LoadingController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { environment } from './../../../../../environments/environment.prod';
import { Storage } from '@ionic/storage';
import { GetApiService } from 'src/app/shared/services/get-api.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { PostApiService } from 'src/app/shared/services/post-api.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  USER_ID = environment.user_id;
  
  infoForm: FormGroup;
  conditionForm: FormGroup;

  isEditItems: boolean;
  isEditConditions: boolean;

  analyzerId: string;
  infos: any;
  condition: any;
  limit: any;

  maxState: boolean = false;
  minState: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private getapi: GetApiService,
    private postapi: PostApiService,
    public storage: Storage,
    private activatedRoute: ActivatedRoute,
    public loadingController: LoadingController,
    public alertCtrl: AlertController,
    private toastController: ToastController,

  ) { 
    this.analyzerId = this.activatedRoute.snapshot.paramMap.get('analyzerId');
 
    this.infoForm =  this.formBuilder.group({
      value_max: [''],
      value_min: [''],
    });

    this.conditionForm =  this.formBuilder.group({
      datetime: [''],
      fishes: [''],
      length: [''],
      weight: ['']
    });

  }


  ngOnInit() {

    this.getapi.getInfoBasic(this.analyzerId).subscribe((res: any) => {
      this.infos = res;
    });
    this.getapi.getInfoCondition(this.analyzerId).subscribe((res: any) => {
      if(Boolean(res[0])) {
        this.condition = res[0];
      }
    });

    this.storage.ready().then(() => {
      this.storage.get(this.USER_ID).then(userId => {
        this.getapi.getInfoLimits(this.analyzerId, userId).subscribe((res: any) => {
          if(Boolean(res[0])) {
            this.limit = res[0];
            this.maxState = res[0].bool_max;
            this.minState = res[0].bool_min;  
          }
          
        });
      });
    });

  }

  onEditCloseItems() {
    this.isEditItems = !this.isEditItems;
  }

  onEditCloseConditions() {
    this.isEditConditions = !this.isEditConditions;
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
        message: msg,
        position: 'bottom',
        duration: 2500
    });
    toast.present();
  }


  maxToggleChanged() {
    this.storage.get(this.USER_ID).then(userId => {
      let value = {analyzer_id: this.analyzerId, position: "max", user_id: userId, bool: this.maxState};
      this.postapi.booleanLimits(value).subscribe();

    });
  }

  minToggleChanged() {
    this.storage.get(this.USER_ID).then(userId => {
      let value = {analyzer_id: this.analyzerId, user_id: userId, position: "min", bool: this.minState};
      this.postapi.booleanLimits(value).subscribe();

    });
  }

  onSubmit() {
    this.storage.get(this.USER_ID).then(userId => {
      this.postapi.updateLimits(this.analyzerId, userId, this.infoForm.value).subscribe((res: any) => {
        this.presentToast(res.success);
        this.onEditCloseItems();
        this.getapi.getInfoLimits(this.analyzerId, userId).subscribe((res: any) => {
          this.limit = res[0];
        });
      });
    });
  }

  onConditionSubmit() {

    let datetime = this.conditionForm.controls['datetime'].value;
    const fishes = this.conditionForm.controls['fishes'].value;
    const length = this.conditionForm.controls['length'].value;
    const weight = this.conditionForm.controls['weight'].value;

    datetime = datetime.split("T", 1)[0];
    const value = {'datetime': datetime, 'fishes': fishes, 'length': length, 'weigth': weight}

    this.postapi.updateCondition(this.analyzerId, value).subscribe((res: any) => {
      this.presentToast(res.success);
      this.onEditCloseConditions();
      this.getapi.getInfoCondition(this.analyzerId).subscribe((res: any) => {
        this.condition = res[0];
      });
    });
  }

  async analyzerPrompt(id, name) {
    const alert = await this.alertCtrl.create({
      header: '수조명 변경',
      inputs: [
        {
          name: 'name_tag',
          value: name,
          placeholder: '수조명입력'
        },
      ],
      buttons: [
        {
          text: '취소',
          role: 'cancel'
        },
        {
          text: '확인',
          role: 'submit',
          handler: data => {
            if(data.name_tag != "") {
              let value = { "name": data.name_tag };
              this.postapi.editAnalyzerName(id, value).subscribe( (res: any) => {  
                this.presentToast(res.success);
                this.getapi.getInfoBasic(this.analyzerId).subscribe((res: any) => {
                  this.infos = res;
                });
              });
            } else {
              return false;
            }
          }
        }
      ]
    });
    await alert.present();
  }


}
