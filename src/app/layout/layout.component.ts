import { FcmApiService } from './../shared/services/fcm-api.service';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { Router } from '@angular/router';
import { MenuController, AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment.prod';
import { FCM } from '@ionic-native/fcm/ngx';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  providers: [InAppBrowser],

})
export class LayoutComponent implements OnInit {
  
  USERNAME = environment.username;
  HOMEPAGE = environment.homepage;

  user: string;
  options: InAppBrowserOptions = {
    zoom: 'no'
  }

  constructor(
    public menu: MenuController,
    private storage: Storage,
    private fcm: FCM,
    private fcmapi: FcmApiService,
    public alertCtrl: AlertController,
    public inAppBrowser: InAppBrowser,

  ) {

   }

  ngOnInit() {
    this.storage.get(this.USERNAME).then(username => {
      
      this.user = username;
      this.fcm.getToken().then(token => {
        if(token) {
          let value = {"username": username, "token": token};
          
          this.fcmapi.updateToken(value).subscribe();
        }
      });
    });

  }

  openUrl() {
    this.inAppBrowser.create(
      `http://fishtech.co.kr`,
      '_blank'
    );
  }


  showAlert(msg) {
    let alert = this.alertCtrl.create({
      message: msg,
      header: '알림',
      buttons: ['OK']
    });
    alert.then(alert => alert.present());
  }

}
